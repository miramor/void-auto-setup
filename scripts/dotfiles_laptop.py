#!/usr/bin/env python3

import glob
from distutils.dir_util import copy_tree

for x in glob.glob("resources/dotfiles/.*"):
    copy_tree(x, os.environ["HOME"])
