#!/usr/bin/env python3

import subprocess, sys

cmd = ["sudo", "xbps-install", "-y"]
for arg in ["-Su", "void-repo-nonfree", "-Su"]:
    subprocess.run(cmd + [arg]) 
with open(sys.argv[1], mode='r') as pkglist:
    pkgs = pkglist.read().splitlines()
    subprocess.run(cmd + pkgs)
