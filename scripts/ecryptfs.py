#!/usr/bin/env python3

import subprocess

cmds = ["sudo xbps-install -Syu",
        "sudo xbps-install -y lsof ecryptfs-utils",
        "sudo cp -r resources/configs/pam.d /etc/",
        "sudo modprobe ecryptfs"]

for x in cmds:
	subprocess.run(x.split(" "))
