#!/usr/bin/env python3
# Force X to use libinput instead of synaptics driver for touchpads

import subprocess

subprocess.run("sudo mkdir -p /etc/X11/xorg.conf.d".split(" "))
subprocess.run(["sudo", "cp", "-f",
                "/usr/share/X11/xorg.conf.d/40-libinput.conf",
                "/etc/X11/xorg.conf.d/"])
