#!/usr/bin/env python3

import subprocess

cmds = ["sudo xbps-install -y flatpak".split(" "),
       ["sudo", "flatpak", "remote-add", "--if-not-exists",
        "flathub", "https://flathub.org/repo/flathub.flatpakrepo"],
        "sudo flatpak update".split(" ")]

for x in cmds:
    subprocess.run(x)
