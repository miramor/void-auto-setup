#!/usr/bin/env python3

import subprocess

app_script = "./resources/flatpak_run.sh"
apps = subprocess.check_output(["flatpak", "list", "--app"]).splitlines()
app_names = []
for x in apps:
    elems = x.split(b'/')
    subprocess.run(["sudo", "cp", app_script,
                    '/'.join(["/usr/local/bin", elems[0].decode("utf-8")])])
