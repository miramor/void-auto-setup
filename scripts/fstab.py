#!/usr/bin/env python3
# Filesystem optimizations for SSDs

import subprocess

subprocess.run(["sudo", "sed", "-i",
                "s/ext4 defaults /ext4 defaults,noatime,discard /g",
                "/etc/fstab"])
