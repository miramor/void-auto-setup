#!/usr/bin/env python3

import subprocess

firewall_cmds = ["sudo ufw enable".split(" "),
                ["sudo", "ln", "-sf", "/etc/sv/ufw",
                 "/etc/runit/runsvdir/default"]]
for x in firewall_cmds:
    subprocess.run(x)

services = ["acpid",
            "adb",
            "alsa",
            "chronyd",
            "cupsd",
            "dbus"]
for x in services:
    subprocess.run(["sudo", "ln", "-sf",
                    ''.join(["/etc/sv/", x]),
                    "/etc/runit/runsvdir/default/"])
