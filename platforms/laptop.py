#!/usr/bin/env python3

import subprocess

subprocess.run("./scripts/fstab.py")
subprocess.run("sudo xbps-install -Syu".split(" "))
subprocess.run(["./scripts/ecryptfs.py"])
subprocess.run(["./scripts/packages.py",
                "./resources/packages/packages_laptop.txt"])

scripts = [''.join(("./scripts/", x)) for x in ["services_laptop.py",
                                                "dotfiles_laptop.py",
                                                "flatpak.py"]]
for x in scripts:
    subprocess.run(x)

subprocess.run(["sudo", "flatpak", "install", "-y", "flathub",
                "org.signal.Signal"])
subprocess.run(["./scripts/flatpak_app_setup.py"])

subprocess.run("sudo cp resources/rc.local_laptop /etc/rc.local".split(" "))
subprocess.run(["./scripts/touchpad_laptop.py"])
