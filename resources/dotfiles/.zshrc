# .bashrc
alias ls='ls --color=auto'

if [ -d ~/google-cloud-sdk ]; then
	source ~/google-cloud-sdk/path.bash.inc
	source ~/google-cloud-sdk/completion.bash.inc
fi

if [ -d ~/.cargo ]; then
	source ~/.cargo/env
fi

if [ ! "$DISPLAY" ] && [ $(tty) = "/dev/tty1" ]; then
	exec ssh-agent startx
fi

autoload -Uz compinit
compinit

export PATH="$PATH:$HOME/.local/bin"
