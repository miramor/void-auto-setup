# .bashrc

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '

if [ -d ~/google-cloud-sdk ]; then
	source ~/google-cloud-sdk/path.bash.inc
	source ~/google-cloud-sdk/completion.bash.inc
fi

if [ -d ~/.cargo ]; then
	source ~/.cargo/env
fi

if [ ! "$DISPLAY" ] && [ $(tty) = "/dev/tty1" ]; then
	exec ssh-agent startx
fi

export PATH="$PATH:$HOME/.local/bin"
