#!/bin/bash -
(
date
acpi -b
acpi -t
read -r -d '' awk_status <<'END'
/^ssid/ {
	print "WiFi: " $2;
}
/^ip_address/ {
	print "WiFi IP: " $2;
}
END
wpa_cli status | awk -F '=' "$awk_status"
) | dmenu -l 5
